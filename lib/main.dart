import 'package:flutter/material.dart';

// import 'package:flutter/src/widgets/container.dart';
enum APP_THEME { LIGHT, DARK }

void main() {
  runApp(ContactProfilePage());
}

class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
          color: Colors.brown,
          iconTheme: IconThemeData(
            color: Colors.white30,
          ),
        ),
        iconTheme: IconThemeData(color: Colors.white60));
  }

  static ThemeData appThemeDark() {
    return ThemeData(
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
          color: Colors.grey,
          iconTheme: IconThemeData(
            color: Colors.white30,
          ),
        ),
        iconTheme: IconThemeData(color: Colors.white70));
  }
}

class ContactProfilePage extends StatefulWidget {

  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}
  class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK
          ? MyAppTheme.appThemeLight()
          : MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton(
          child:Icon(Icons.add),
          onPressed: () {
            setState(() {
              currentTheme == APP_THEME.DARK
                  ? currentTheme = APP_THEME.LIGHT
                  : currentTheme = APP_THEME.DARK;
            });
          },
        ),
      ),
    );
  }

}

Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
          //color: Colors.brown.shade400,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,
          //color: Colors.brown.shade400,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
          //color: Colors.brown.shade400,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
          //color: Colors.brown.shade400,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}

Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          //color: Colors.brown.shade400,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
          //color: Colors.brown.shade400,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}

Widget mobilePhoneLisTile() {
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("095-727-7972"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.brown.shade400,
      onPressed: () {},
    ),
  );
}

Widget ortherPhoneListTile() {
  return ListTile(
    leading: Text(""),
    title: Text("02-440-3390"),
    subtitle: Text("other"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.brown.shade400,
      onPressed: () {},
    ),
  );
}

Widget emailListTile() {
  return ListTile(
    leading: Icon(Icons.email),
    title: Text("63160216@go.buu.ac.th"),
    subtitle: Text("study"),
    trailing: Text(""),
  );
}

Widget addressListTile() {
  return ListTile(
    leading: Icon(Icons.location_pin),
    title: Text("16/124 Bandsean, Chonburi"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(Icons.directions),
      color: Colors.brown.shade400,
      onPressed: () {},
    ),
  );
}

AppBar buildAppBarWidget() {
  return AppBar(
    backgroundColor: Colors.brown.shade400,
    leading: Icon(Icons.arrow_back),
    actions: <Widget>[
      IconButton(onPressed: () {}, icon: Icon(Icons.star_border))
    ],
  );
}

Widget profileActionItems() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoCallButton(),
      buildEmailButton(),
      buildDirectionsButton(),
      buildPayButton()
    ],
  );
}

Widget buildBodyWidget() {
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(
            width: double.infinity,
            //Height constraint at Container widget level
            height: 250,
            child: Image.network(
              "https://static1.colliderimages.com/wordpress/wp-content/uploads/2021/09/jujutsu-kaisen-Gojo.jpg",
              fit: BoxFit.cover,
            ),
          ),
          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "Gojo Satoru",
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.grey,
          ),
          Container(
            margin: EdgeInsets.only(top: 8, bottom: 8),
            child: Theme(
              data: ThemeData(
                  iconTheme: IconThemeData(
                    color: Colors.brown,
                  )),
              child: profileActionItems(),
            ),
          ),
          Divider(
            color: Colors.grey,
          ),
          mobilePhoneLisTile(),
          ortherPhoneListTile(),
          Divider(
            color: Colors.grey,
          ),
          emailListTile(),
          Divider(
            color: Colors.grey,
          ),
          addressListTile()
        ],
      )
    ],
  );
}
